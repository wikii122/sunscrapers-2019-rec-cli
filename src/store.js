import Vue from "vue";
import Vuex from "vuex";

import axios from "axios";
Vue.use(Vuex);

export default new Vuex.Store({
  getters: {
    activeCompany(state) {
      return state.activeCompany;
    },
    companies(state) {
      return state.companies;
    }
  },
  state: {
    activeCompany: null,
    companies: null,
  },
  mutations: {
    activeCompany(state, newSymbol) {
      state.activeCompany = newSymbol;
    },
    companiesList(state, newList) {
      state.companies = newList;
    }
  },
  actions: {
    async createCompany(context, data) {
      if (!data.symbol) return;
      const response = await axios.post('http://localhost:8000/companies/', data);
      setTimeout(() => context.dispatch('updateCompanies'), 2000);
    },
    async updateCompanies(context) {
      const response = await axios.get('http://localhost:8000/companies/');
      const data = response.data;
      context.commit('companiesList', data.results);
    },
    showCompany(context, symbol) {
      context.commit('activeCompany', symbol);
    },
  },
});
